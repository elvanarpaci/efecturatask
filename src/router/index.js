import Vue from 'vue'
import Router from 'vue-router'
import ShoppingBag from '@/components/ShoppingBag'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'ShoppingBag',
      component: ShoppingBag
    }
  ]
})
